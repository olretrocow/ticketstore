package com.dataart.ticketstore.util;

import com.dataart.ticketstore.dto.FlightDTO;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;


@Service
public class TimeHandlerUtil {
    public LocalDateTime getArrivalTime(FlightDTO flightDTO) {
        return flightDTO.getDepartureTime()
                .plus(flightDTO.getFlightTime().getHour(), ChronoUnit.HOURS)
                .plus(flightDTO.getFlightTime().getMinute(), ChronoUnit.MINUTES)
                .plus(flightDTO.getFlightTime().getSecond(), ChronoUnit.SECONDS);
    }
}
