package com.dataart.ticketstore.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class FlightsFilterDTO {
    private Integer page;
    private Integer size;
    private Long departureCityId;
    private Long arrivalCityId;
    private Long airlineId;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dayFrom;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dayTo;
}
