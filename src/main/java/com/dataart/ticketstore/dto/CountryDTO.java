package com.dataart.ticketstore.dto;

import lombok.Data;


@Data
public class CountryDTO {
    private Long id;
    private String name;
}
