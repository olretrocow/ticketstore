package com.dataart.ticketstore.dto;

import lombok.Data;

@Data
public class AirlineDTO {
    private String name;
    private String logo;
}
