package com.dataart.ticketstore.dto;

import com.dataart.ticketstore.validation.annotation.InfoMatch;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@InfoMatch(
        info = "password",
        infoMatch = "passwordConfirm",
        message = "Passwords do not match!"
)
public class UserCredentialsDTO {
    private Long userId;
    @NotEmpty
    @Email
    private String email;
    @NotEmpty
    @Size(min = 6, max = 30, message = "Password must be longer than 6 and shorter then 30 characters")
    private String password;
    private String passwordConfirm;
    private String newPassword;
}
