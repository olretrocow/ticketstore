package com.dataart.ticketstore.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
public class FlightDTO {
    private Long id;
    private CityDTO departureCity;
    private CityDTO arrivalCity;
    private LocalDateTime departureTime;
    @DateTimeFormat(pattern = "HH:mm")
    private LocalTime flightTime;
    private AirlineDTO airline;
}
