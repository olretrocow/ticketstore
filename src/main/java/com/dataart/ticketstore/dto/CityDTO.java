package com.dataart.ticketstore.dto;

import lombok.Data;

@Data
public class CityDTO {
    private Long id;
    private CountryDTO country;
    private String name;
}
