package com.dataart.ticketstore.dto;

import com.dataart.ticketstore.entity.Sex;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
public class UserDTO {
    private Long id;
    @NotEmpty(message = "Enter your name")
    @Size(min = 2, max = 40, message = "Name must be longer than 2 and shorter than 40 characters")
    private String name;
    @NotEmpty(message = "Enter your surname")
    @Size(min = 2, max = 40, message = "Surname must be longer than 2 and shorter than 40 characters")
    private String surname;
    private String patronymic;
    @Size(min = 3, max = 40, message = "Field is too long")
    @Email
    private String email;
    @Size(min = 6, max = 30, message = "Password must be longer than 6 and shorter then 30 characters")
    private String password;
    @NotNull(message = "Enter your birthdate")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthdate;
    @NotEmpty(message = "Enter your passport")
    @Pattern(regexp = "^\\d{4}\\s\\d{6}", message = "Enter your passport in proper format")
    private String passport;
    @NotEmpty(message = "Enter your phone")
    @Pattern(regexp = "^\\+[7]\\d{10}", message = "Enter your phone in format +79999999999")
    private String phone;
    @NotNull(message = "Choose your gender")
    @Enumerated(EnumType.STRING)
    private Sex sex;
    private CountryDTO citizenship;
    @NotNull(message = "Choose your citizenship")
    private Long citizenshipId;
    private String passwordConfirm;
}
