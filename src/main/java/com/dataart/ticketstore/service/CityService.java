package com.dataart.ticketstore.service;

import com.dataart.ticketstore.dto.CityDTO;
import com.dataart.ticketstore.entity.City;
import com.dataart.ticketstore.mapper.CityMapper;
import com.dataart.ticketstore.repository.CityRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CityService {
    private final CityRepository cityRepository;
    private final CityMapper cityMapper;

    public List<CityDTO> getAll() {
        return cityRepository.findAll().stream().map(cityMapper::toDTO).toList();
    }
}
