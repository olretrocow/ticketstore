package com.dataart.ticketstore.service;

import com.dataart.ticketstore.dto.FlightDTO;
import com.dataart.ticketstore.dto.FlightsFilterDTO;
import com.dataart.ticketstore.dto.UserDTO;
import com.dataart.ticketstore.entity.Flight;
import com.dataart.ticketstore.entity.User;
import com.dataart.ticketstore.exception.InfoMatchException;
import com.dataart.ticketstore.exception.UserNotFoundException;
import com.dataart.ticketstore.mapper.FlightMapper;
import com.dataart.ticketstore.mapper.UserMapper;
import com.dataart.ticketstore.repository.FlightRepository;
import com.dataart.ticketstore.security.service.UserDetailsImpl;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class FlightService {
    private final FlightRepository flightRepository;
    private final FlightMapper flightMapper;

    private final UserMapper userMapper;
    private final UserService userService;
    @Value("${application.defaultStartPage:0}")
    private int defaultPage;
    @Value("${application.defaultPageSize:5}")
    private int defaultPageSize;

    private FlightsFilterDTO flightsFilterValidate(FlightsFilterDTO flightsFilterDTO) {
        int page = flightsFilterDTO.getPage() == null ? defaultPage : flightsFilterDTO.getPage();
        int size = flightsFilterDTO.getSize() == null ? defaultPageSize : flightsFilterDTO.getSize();
        flightsFilterDTO.setPage(page);
        flightsFilterDTO.setSize(size);
        return flightsFilterDTO;
    }

    public Page<FlightDTO> getFlightPage(FlightsFilterDTO flightsFilterDTO) {
        var flightsFilter = flightsFilterValidate(flightsFilterDTO);
        Page<Flight> flightPage = flightRepository.findByFilter(
                flightsFilter.getDepartureCityId(),
                flightsFilter.getArrivalCityId(),
                flightsFilter.getAirlineId(),
                flightsFilter.getDayFrom().toString(),
                flightsFilter.getDayTo().toString(),
                PageRequest.of(flightsFilter.getPage(), flightsFilter.getSize()));
        return flightPage.map(flightMapper::toDTO);
    }

    public Page<FlightDTO> getUserFlightPage(FlightsFilterDTO flightsFilterDTO) {
        var flightsFilter = flightsFilterValidate(flightsFilterDTO);
        Page<Flight> flightPage = flightRepository.findAllByUser(userService.getAuthenticatedUser().orElseThrow(UserNotFoundException::new).getId(), PageRequest.of(flightsFilter.getPage(), flightsFilter.getSize()));
        return flightPage.map(flightMapper::toDTO);
    }

    public FlightDTO getById(long id) {
        return flightMapper.toDTO(flightRepository.findById(id).orElseThrow(UserNotFoundException::new));
    }

    public FlightDTO addUserFlight(UserDTO userDTO, Long flightId) {
        Object authUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Flight flight = flightRepository.findById(flightId).orElseThrow(UserNotFoundException::new);
        if (flight.getTicketQuantity() == 0) {
            throw new InfoMatchException("Sorry, tickets for this flight are over");
        }
        if (authUser == "anonymousUser") {
            UserDTO savedUser = userService.addUser(userDTO);
            if (savedUser != null) {
                flight.getUsers().add(userMapper.toEntity(savedUser));
                flight.setTicketQuantity(flight.getTicketQuantity() - 1);
                return flightMapper.toDTO(flightRepository.save(flight));
            }
            throw new InfoMatchException("Registration Error");
        }
        UserDetailsImpl userDetails = (UserDetailsImpl) authUser;
        flight.getUsers().add(userMapper.toEntity(userService.getUserByUsername(userDetails.getUsername()).orElseThrow(UserNotFoundException::new)));
        flight.setTicketQuantity(flight.getTicketQuantity() - 1);
        return flightMapper.toDTO(flightRepository.save(flight));
    }
}
