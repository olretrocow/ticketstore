package com.dataart.ticketstore.service;

import com.dataart.ticketstore.dto.CountryDTO;
import com.dataart.ticketstore.mapper.CountryMapper;
import com.dataart.ticketstore.repository.CountryRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CountryService {
    private final CountryRepository countryRepository;
    private final CountryMapper countryMapper;
    public List<CountryDTO> getAll() {
        return countryRepository.findAll().stream().map(countryMapper::toDTO).toList();
    }

}
