package com.dataart.ticketstore.service;

import com.dataart.ticketstore.dto.JwtTokenDTO;
import com.dataart.ticketstore.dto.UserCredentialsDTO;
import com.dataart.ticketstore.dto.UserDTO;
import com.dataart.ticketstore.dto.UserInfoDTO;
import com.dataart.ticketstore.entity.User;
import com.dataart.ticketstore.exception.InfoMatchException;
import com.dataart.ticketstore.exception.UserNotFoundException;
import com.dataart.ticketstore.mapper.UserMapper;
import com.dataart.ticketstore.repository.CountryRepository;
import com.dataart.ticketstore.repository.RoleRepository;
import com.dataart.ticketstore.repository.UserRepository;
import com.dataart.ticketstore.security.jwt.JwtUtils;
import com.dataart.ticketstore.security.service.UserDetailsImpl;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional(isolation = Isolation.REPEATABLE_READ)
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final RoleRepository roleRepository;
    private final PasswordEncoder bCryptPasswordEncoder;
    private final CountryRepository countryRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;

    public JwtTokenDTO authenticateUser(UserCredentialsDTO userCredentialsDTO) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userCredentialsDTO.getEmail(), userCredentialsDTO.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        return new JwtTokenDTO(jwt, userDetails.getId(), userDetails.getUsername(), roles);
    }

    public Optional<UserDTO> getUserByUsername(String email) {

        Optional<User> user = userRepository.findByEmail(email.toLowerCase());

        if (user.isEmpty()) {
            LOGGER.debug("Failed to get user by Email " + email);
            throw new UsernameNotFoundException("User not found");
        }

        return Optional.ofNullable(userMapper.toDTO(user.get()));
    }

    public Optional<UserDTO> getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) {
            return Optional.empty();
        }
        return Optional.ofNullable(userMapper.toDTO((User) authentication.getPrincipal()));
    }

    public Optional<UserInfoDTO> getAuthenticatedUserInfo() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) {
            return Optional.empty();
        }
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        return Optional.ofNullable(userMapper.toInfoDTO(userRepository.getById(user.getId())));
    }

    private boolean checkId(Long userId) throws UserNotFoundException {
        return getAuthenticatedUser().orElseThrow(UserNotFoundException::new).getId().equals(userId);
    }

    public UserDTO changePassword(UserCredentialsDTO userDTO) {
        if (userDTO == null) {
            throw new UserNotFoundException("User is not found");
        }
        User user = userRepository.findById(userDTO.getUserId()).orElseThrow(UserNotFoundException::new);
        if (!bCryptPasswordEncoder.matches(userDTO.getPassword(), user.getPassword()) || Objects.equals(userDTO.getPasswordConfirm(), userDTO.getPassword())) {
            LOGGER.debug("Failed to change password for user " + userDTO.getEmail());
            throw new InfoMatchException("Passwords are not match");
        }
        if (checkId(userDTO.getUserId())) {
            user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPasswordConfirm()));
            userRepository.save(user);
            return userMapper.toDTO(user);
        }
        LOGGER.debug("Failed to change password for user " + userDTO.getEmail());
        throw new InfoMatchException("Users are not match");
    }

    public UserDTO updateUser(UserInfoDTO userInfoDTO) {
        if (userInfoDTO == null) {
            throw new UserNotFoundException("User is not found");
        }
        User user = userRepository.findById(userInfoDTO.getId()).orElseThrow(() -> new UserNotFoundException("User is not found"));
        if (checkId(userInfoDTO.getId())) {
            userMapper.update(userInfoDTO, user);
            user.setCitizenship(countryRepository.getById(userInfoDTO.getCitizenshipId()));
            userRepository.save(user);
            return userMapper.toDTO(user);
        }
        LOGGER.debug("Failed to change password for user " + userInfoDTO.getEmail());
        throw new InfoMatchException("Users are not match");
    }

    public UserDTO addUser(UserDTO userDTO) {
        Optional<User> userFromDB = userRepository.findByEmail(userDTO.getEmail().toLowerCase());
        if (userFromDB.isPresent()) {
            LOGGER.debug("Attempt to register already registered user " + userFromDB.get().getEmail());
            throw new InfoMatchException("User with this Email is already registered");
        }
        User user = userMapper.toEntity(userDTO);
        user.setEmail(user.getEmail().toLowerCase());
        user.setCitizenship(countryRepository.getById(userDTO.getCitizenshipId()));
        user.setRoles(Collections.singleton(roleRepository.getById(1L)));
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        user.setEnabled(true);
        userRepository.save(user);
        return userMapper.toDTO(user);
    }

    public void validatePasswords(UserDTO userDTO) {
        if (userDTO.getPassword().isEmpty()) {
            throw new InfoMatchException("Password is empty");
        }
        if (!userDTO.getPassword().equals(userDTO.getPasswordConfirm())) {
            throw new InfoMatchException("Passwords do not match");
        }
    }

    public UserCredentialsDTO getChangePasswordDTO(UserDTO userDTO) {
        return userMapper.toCredentialsDTO(userRepository.findByEmail(userDTO.getEmail().toLowerCase()).orElseThrow(UserNotFoundException::new));
    }
}
