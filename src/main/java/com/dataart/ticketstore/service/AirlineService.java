package com.dataart.ticketstore.service;

import com.dataart.ticketstore.dto.AirlineDTO;
import com.dataart.ticketstore.mapper.AirlineMapper;
import com.dataart.ticketstore.repository.AirlineRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AirlineService {
    private final AirlineRepository airlineRepository;
    private final AirlineMapper airlineMapper;

    public List<AirlineDTO> getAll() {
        return airlineRepository.findAll().stream().map(airlineMapper::toDTO).toList();
    }
}
