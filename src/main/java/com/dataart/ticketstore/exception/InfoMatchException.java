package com.dataart.ticketstore.exception;

public class InfoMatchException extends RuntimeException {

    public InfoMatchException(String message) {
        super(message);
    }

}
