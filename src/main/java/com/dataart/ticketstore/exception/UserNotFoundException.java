package com.dataart.ticketstore.exception;

import lombok.Getter;

@Getter
public class UserNotFoundException extends RuntimeException {
    private String message;

    public UserNotFoundException() {
        message = "User is not found";
    }
    public UserNotFoundException(String message) {
        super(message);
    }
}
