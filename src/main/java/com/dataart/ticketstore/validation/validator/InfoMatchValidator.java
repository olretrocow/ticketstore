package com.dataart.ticketstore.validation.validator;

import com.dataart.ticketstore.exception.InfoMatchException;
import com.dataart.ticketstore.validation.annotation.InfoMatch;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class InfoMatchValidator implements ConstraintValidator<InfoMatch, Object> {

    private String info;
    private String infoMatch;

    @Override
    public void initialize(InfoMatch constraintAnnotation) {
        this.info = constraintAnnotation.info();
        this.infoMatch = constraintAnnotation.infoMatch();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        Object infoValue = new BeanWrapperImpl(o)
                .getPropertyValue(info);
        Object infoMatchValue = new BeanWrapperImpl(o)
                .getPropertyValue(infoMatch);

        return !(infoValue == null) && infoValue.equals(infoMatchValue);
    }
}
