package com.dataart.ticketstore.validation.annotation;

import com.dataart.ticketstore.validation.validator.InfoMatchValidator;

import javax.validation.Constraint;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = InfoMatchValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface InfoMatch {

    String message() default "Info values don't match!";
    String info();
    String infoMatch();
}
