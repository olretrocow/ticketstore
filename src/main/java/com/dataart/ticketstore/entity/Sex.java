package com.dataart.ticketstore.entity;

public enum Sex {
    MALE,
    FEMALE
}
