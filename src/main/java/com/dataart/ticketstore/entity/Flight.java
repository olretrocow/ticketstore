package com.dataart.ticketstore.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Set;

@Entity
@Table(name = "flight")
@Getter
@Setter
public class Flight {
    @Id
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "departure_city_id", nullable = false)
    private City departureCity;

    @ManyToOne(optional = false)
    @JoinColumn(name = "arrival_city_id", nullable = false)
    private City arrivalCity;

    private LocalDateTime departureTime;

    private LocalTime flightTime;

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "airline_id", nullable = false)
    private Airline airline;

    @ManyToMany
    @JoinTable(
            name = "t_user_flights",
            joinColumns = {@JoinColumn(name = "flight_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> users;

    private Integer ticketQuantity;
}
