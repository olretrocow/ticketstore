package com.dataart.ticketstore.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "airline")
@Data
public class Airline {
    @Id
    private Long id;
    private String name;
    private String logo;
}
