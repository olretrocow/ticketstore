package com.dataart.ticketstore.controller;

import com.dataart.ticketstore.dto.AirlineDTO;
import com.dataart.ticketstore.dto.CityDTO;
import com.dataart.ticketstore.dto.CountryDTO;
import com.dataart.ticketstore.service.AirlineService;
import com.dataart.ticketstore.service.CityService;
import com.dataart.ticketstore.service.CountryService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000/")
@AllArgsConstructor
public class ResourceController {
    private final CountryService countryService;
    private final CityService cityService;
    private final AirlineService airlineService;

    @GetMapping("/countries")
    public ResponseEntity<List<CountryDTO>> getCountries() {
        return ResponseEntity.ok(countryService.getAll());
    }

    @GetMapping("/cities")
    public ResponseEntity<List<CityDTO>> getCities() {
        return ResponseEntity.ok(cityService.getAll());
    }

    @GetMapping("/airlines")
    public ResponseEntity<List<AirlineDTO>> getAirlines() {
        return ResponseEntity.ok(airlineService.getAll());
    }
}
