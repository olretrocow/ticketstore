package com.dataart.ticketstore.controller;

import com.dataart.ticketstore.dto.*;
import com.dataart.ticketstore.exception.UserNotFoundException;
import com.dataart.ticketstore.security.jwt.JwtUtils;
import com.dataart.ticketstore.security.service.UserDetailsImpl;
import com.dataart.ticketstore.service.FlightService;
import com.dataart.ticketstore.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("http://localhost:3000/")
@AllArgsConstructor
public class UserController {

    private final UserService userService;
    private final FlightService flightService;

    @GetMapping("/user/info")
    public ResponseEntity<UserInfoDTO> info() {
        return ResponseEntity.ok(userService.getAuthenticatedUserInfo().orElseThrow(() -> new UserNotFoundException("User is not found")));
    }

    @PostMapping("/user/change-info")
    public ResponseEntity<UserDTO> changeInfo(@RequestBody @Valid UserInfoDTO userInfoDTO) {
        return ResponseEntity.ok(userService.updateUser(userInfoDTO));
    }

    @PostMapping("/user/change-password")
    public ResponseEntity<UserDTO> changePassword(@RequestBody UserCredentialsDTO userDTO) {
        userService.changePassword(userDTO);
        return ResponseEntity.ok(userService.changePassword(userDTO));
    }

    @PostMapping("/user/flights")
    public ResponseEntity<Page<FlightDTO>> userFlights(@RequestBody FlightsFilterDTO flightsFilterDTO) {
        return ResponseEntity.ok(flightService.getUserFlightPage(flightsFilterDTO));
    }

    @GetMapping("/users/new")
    public ResponseEntity<UserDTO> registration() {
        return ResponseEntity.ok(new UserDTO());
    }

    @PostMapping("/users/new")
    public ResponseEntity<UserDTO> addUser(@RequestBody UserDTO userDTO) {
        userService.validatePasswords(userDTO);
        return ResponseEntity.ok(userService.addUser(userDTO));
    }

    @PostMapping("/user/login")
    public ResponseEntity<JwtTokenDTO> loginUser(@Valid @RequestBody UserCredentialsDTO userCredentialsDTO) {
        return ResponseEntity.ok(userService.authenticateUser(userCredentialsDTO));
    }
}
