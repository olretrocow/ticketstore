package com.dataart.ticketstore.controller;

import com.dataart.ticketstore.dto.FlightDTO;
import com.dataart.ticketstore.dto.FlightsFilterDTO;
import com.dataart.ticketstore.dto.UserDTO;
import com.dataart.ticketstore.service.FlightService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@CrossOrigin("http://localhost:3000/")
@AllArgsConstructor
public class FlightController {
    private final FlightService flightService;


    @PostMapping("/flights")
    public ResponseEntity<Page<FlightDTO>> index(@RequestBody FlightsFilterDTO flightsFilterDTO) {
        return ResponseEntity.ok(flightService.getFlightPage(flightsFilterDTO));
    }

    @GetMapping("flight/{id}")
    public ResponseEntity<FlightDTO> getFlight(@PathVariable Long id) {
        return ResponseEntity.ok(flightService.getById(id));
    }

    @PostMapping("flight/add-user-flight")
    public ResponseEntity<FlightDTO> addUserFlight(@RequestBody @Valid UserDTO userDTO,
                                                   @RequestParam(name = "flightId") Long flightId) {
        return ResponseEntity.ok(flightService.addUserFlight(userDTO, flightId));
    }
}
