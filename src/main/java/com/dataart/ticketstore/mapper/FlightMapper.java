package com.dataart.ticketstore.mapper;

import com.dataart.ticketstore.dto.FlightDTO;
import com.dataart.ticketstore.entity.Flight;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface FlightMapper {
    FlightDTO toDTO(Flight flight);

    @Mapping(target = "airline", ignore = true)
    Flight toEntity(FlightDTO flightDTO);
}
