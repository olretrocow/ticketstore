package com.dataart.ticketstore.mapper;

import com.dataart.ticketstore.dto.CountryDTO;
import com.dataart.ticketstore.entity.Country;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CountryMapper {
    CountryDTO toDTO(Country country);
}
