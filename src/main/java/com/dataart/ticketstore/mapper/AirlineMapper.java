package com.dataart.ticketstore.mapper;

import com.dataart.ticketstore.dto.AirlineDTO;
import com.dataart.ticketstore.entity.Airline;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AirlineMapper {
    AirlineDTO toDTO(Airline airline);
}
