package com.dataart.ticketstore.mapper;

import com.dataart.ticketstore.dto.UserCredentialsDTO;
import com.dataart.ticketstore.dto.UserDTO;
import com.dataart.ticketstore.dto.UserInfoDTO;
import com.dataart.ticketstore.entity.User;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface UserMapper {
    @Mapping(source = "user.citizenship", target = "citizenship")
    @Mapping(source = "user.sex", target = "sex")
    UserDTO toDTO(User user);

    @Mapping(source = "user.id", target = "userId")
    UserCredentialsDTO toCredentialsDTO(User user);

    UserInfoDTO toInfoDTO(User user);

    User toEntity(UserDTO userDTO);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
    )
    @Mapping(target = "citizenship", ignore = true)
    void update(UserInfoDTO userInfoDTO, @MappingTarget User user);
}
