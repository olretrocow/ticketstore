package com.dataart.ticketstore.mapper;

import com.dataart.ticketstore.dto.CityDTO;
import com.dataart.ticketstore.entity.City;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CityMapper {
    @Mapping(source = "city.id", target = "id")
    CityDTO toDTO(City city);
}
