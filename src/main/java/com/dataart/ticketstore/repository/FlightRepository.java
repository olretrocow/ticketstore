package com.dataart.ticketstore.repository;

import com.dataart.ticketstore.entity.Flight;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long> {
    @Query(
            value = "SELECT * FROM flight AS f WHERE " +
                    "(:departureCityId IS NULL OR f.departure_city_id = :departureCityId)" +
                    "AND (:arrivalCityId IS NULL OR f.arrival_city_id = :arrivalCityId)" +
                    "AND (:airlineId IS NULL OR f.airline_id = :airlineId)" +
                    "AND (f.departure_time BETWEEN CAST(CAST(:dayFrom AS CHARACTER VARYING) AS DATE ) AND CAST(CAST(:dayTo AS CHARACTER VARYING) AS DATE ))" +
                    "AND ticket_quantity >= 1", nativeQuery = true
    )
    Page<Flight> findByFilter(Long departureCityId,
                              Long arrivalCityId,
                              Long airlineId,
                              String dayFrom,
                              String dayTo,
                              Pageable pageable);

    @Query(
            value = "SELECT * FROM flight AS f " +
                    "JOIN t_user_flights AS uf ON f.id = uf.flight_id " +
                    "JOIN t_user AS u ON u.id = uf.user_id " +
                    "WHERE u.id = :userId", nativeQuery = true)
    Page<Flight> findAllByUser(long userId, Pageable pageable);

    Optional<Flight> findById(long id);
}
