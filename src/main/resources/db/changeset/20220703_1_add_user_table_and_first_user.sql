--liquibase formatted sql
--changeset nikita_biketov:1
-- runOnChange:true
CREATE TABLE "t_user"
(
    id             BIGSERIAL PRIMARY KEY           NOT NULL,
    name           VARCHAR                         NOT NULL,
    surname        VARCHAR                         NOT NULL,
    patronymic     VARCHAR,
    password       VARCHAR                         NOT NULL,
    citizenship_id INTEGER references country (id) not null,
    birthdate      date,
    phone          varchar                         not null,
    passport       varchar,
    email          varchar UNIQUE                  not null,
    sex            varchar,
    enabled        boolean
);