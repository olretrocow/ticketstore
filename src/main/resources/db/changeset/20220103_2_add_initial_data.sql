--liquibase formatted sql
--changeset nikita_biketov:1
-- runOnChange:true
INSERT INTO country (name) VALUES ('Россия');
INSERT INTO country (name) VALUES ('Китай');
INSERT INTO country (name) VALUES ('Франция');
INSERT INTO city (country_id, name) VALUES
                                        (1, 'Москва'), (1, 'Воронеж'), (1, 'Липецк');
INSERT INTO city (country_id, name) VALUES
                                        (2, 'Пекин'), (2, 'Гонконг'), (2, 'Ухань');
INSERT INTO city (country_id, name) VALUES
                                        (3, 'Париж'), (3, 'Версаль'), (3, 'Марсель');
INSERT INTO flight (departure_city_id, arrival_city_id, departure_time, flight_time, ticket_quantity) values
    (4, 7, '1999-01-08 04:05:06', '04:00:00', 10);
