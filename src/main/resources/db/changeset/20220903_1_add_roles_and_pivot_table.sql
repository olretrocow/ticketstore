--liquibase formatted sql
--changeset nikita_biketov:1
-- runOnChange:true
CREATE TABLE t_role (
    id BIGSERIAL PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL
);
--liquibase formatted sql
--changeset nikita_biketov:2
-- runOnChange:true
CREATE TABLE t_user_roles (
    user_id BIGINT references t_user(id),
    roles_id BIGINT references t_role(id)
);
INSERT INTO t_role (name) VALUES ('ROLE_USER'), ('ROLE_ADMIN');
