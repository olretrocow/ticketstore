--liquibase formatted sql
--changeset nikita_biketov:1
-- runOnChange:true
CREATE TABLE country
(
    id   BIGSERIAL PRIMARY KEY,
    name varchar NOT NULL UNIQUE
);
--liquibase formatted sql
--changeset nikita_biketov:2
-- runOnChange:true
CREATE TABLE city
(
    id         BIGSERIAL PRIMARY KEY,
    country_id BIGINT,
    name       varchar NOT NULL,
    CONSTRAINT fk_customer
        FOREIGN KEY (country_id)
            REFERENCES country (id)
);
--liquibase formatted sql
--changeset nikita_biketov:3
-- runOnChange:true
CREATE TABLE flight
(
    id                BIGSERIAL PRIMARY KEY,
    departure_city_id BIGINT    NOT NULL,
    arrival_city_id   BIGINT    NOT NULL,
    departure_time    timestamp NOT NULL,
    flight_time       time      NOT NULL,
    ticket_quantity   INT       NOT NULL,
    FOREIGN KEY (departure_city_id) REFERENCES city (id),
    FOREIGN KEY (arrival_city_id) REFERENCES city (id)
);