--liquibase formatted sql
--changeset nikita_biketov:1
-- runOnChange:true
CREATE TABLE airline (
    id BIGSERIAL PRIMARY KEY,
    name varchar NOT NULL UNIQUE,
    logo varchar NOT NULL DEFAULT('default-logo.png')
);

--liquibase formatted sql
--changeset nikita_biketov:2
-- runOnChange:true
INSERT INTO airline (name, logo) VALUES ('Ural Airlines', 'ural.png');
ALTER TABLE flight ADD COLUMN airline_id BIGINT;
UPDATE flight SET airline_id = 1;
ALTER TABLE flight ALTER airline_id SET NOT NULL;
ALTER TABLE flight ADD CONSTRAINT fk_airline FOREIGN KEY (airline_id) REFERENCES airline(id);