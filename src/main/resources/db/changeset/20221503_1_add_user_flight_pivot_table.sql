--liquibase formatted sql
--changeset nikita_biketov:1
-- runOnChange:true
CREATE TABLE t_user_flights (
    user_id BIGINT references t_user(id),
    flight_id BIGINT references flight(id)
    );