--liquibase formatted sql
--changeset nikita_biketov:1
-- runOnChange:true
INSERT INTO airline (name, logo)
VALUES ('Аэрофлот', 'aeroflot.png'),
       ('Алроса', 'alrosa.png'),
       ('ИрАэро', 'iraero.png'),
       ('Азимут', 'azimut.png'),
       ('Nordstar', 'nordstar.png'),
       ('Nordwind', 'nordwind.png'),
       ('Pegasus', 'pegasus.png'),
       ('Победа', 'pobeda.png'),
       ('Red Wings', 'redwings.png'),
       ('Rossiya', 'rossiya.png'),
       ('RusLine', 'rusline.png'),
       ('S7 Airlines', 'S7.png'),
       ('Smartavia', 'smartavia.png'),
       ('Якутия', 'yakutia.png'),
       ('Ямал', 'yamal.png');
UPDATE flight SET airline_id = random()*(16-1)+1;